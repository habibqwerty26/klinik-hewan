<?= $this->extend('layout') ?>
<?= $this->section('content') ?>
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title m-0">Dashboard</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <h4 class="m-t-0">Visi & Misi</h4>
                <?php
                        if (session()->getFlashData('success')) {
                        ?>
                <div class="alert alert-success alert-dismissible fade show">
                    <?= session()->getFlashData('success') ?>
                </div>
                <?php } ?>
                <div style="height: 300px"></div>
            </div>
        </div>
    </div>



</div> <!-- end container-fluid -->



<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="m-b-30 m-t-0">Jadwal Praktik Dokter</h4>                
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
                            style="border-collapse: collapse; width: 100%;">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Dokter</th>
                                    <th>Jam Praktik</th>
                                    <th>Keterangan</th>
                                    <!-- <th>Aksi</th> -->
                                </tr>
                            </thead>
                            <!-- ['nama_dokter', 'jam_kerja', 'keterangan']; -->
                            <tbody>
                                <?php $no = 1; ?>
                                <?php foreach ($dokters as $d) { ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $d['nama_dokter']; ?></td>
                                    <td><?= $d['jam_kerja']; ?></td>
                                    <td><?= $d['keterangan']; ?></td>
                                    <td>
                                        <!-- <a class="btn btn-danger btn-sm text-white" href="">Delete</a>
                                <a class="btn btn-warning btn-sm" href="">Edit</a> -->
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div> <!-- End Row -->


<div class="col-xl-3 col-lg-4 col-md-6 m-b-30">
    <button class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#edit-modal" id="edit">Pendaftaran</button>
</div>

<div id="edit-modal" name="edit-modal" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="custom-width-modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title m-0" id="custom-width-modalLabel">Daftar Hewan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <?php $id_pasien = session()->get('id_pasien'); ?>
                <form class="form-horizontal" id="form_id" action="<?=base_url('/pasien/insert_hewan/'. $id_pasien) ?>" method="post">
                    <?= csrf_field(); ?>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 control-label">Nama Hewan</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" id="nama_dokter" placeholder="Nama Hewan"
                                name="nama_hewan">
                        </div>
                    </div>
                    <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-3 control-label">Nama Dokter</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="id_dokter" placeholder="Jenis Kelamin">
                                    <?php foreach($dokters as $dokter) { ?>
                                    <option value="<?=$dokter['id_dokter']?>"><?= $dokter['nama_dokter'] ?></option>
                                    <?php } ?>                                   
                                </select>
                            </div>
                        </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 control-label">Jenis
                            Kelamin</label>
                        <div class="col-sm-9">
                            <!-- <input type="" class="form-control" id="inputPassword3" placeholder="Jenis Kelamin" name="jenkel"> -->
                            <select class="form-control" name="jenkel">
                                <option value="Jantan">Jantan</option>
                                <option value="Betina">Betina</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Spesies</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" id="alamat" placeholder="Spesies" name="spesies">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Umur</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" id="no_tlp" placeholder="umur" name="umur">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Keluhan</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" id="keluhan" placeholder="Keluhan" name="keluhan">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<?= $this->endSection() ?>