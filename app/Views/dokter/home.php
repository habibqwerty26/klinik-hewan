<?= $this->extend('layout') ?>
<?= $this->section('content') ?>
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title m-0">Datatables</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="m-b-30 m-t-0">Default Example</h4>
                <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="modal"
                    data-target="#custom-width-modal">Tambah Dokter</button>
                <?php
                        if (session()->getFlashData('success')) {
                        ?>
                <div class="alert alert-success alert-dismissible fade show">
                    <?= session()->getFlashData('success') ?>
                </div>
                <?php } ?>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
                            style="border-collapse: collapse; width: 100%;">
                            <thead>
                                <tr>
                                    <th>No. </th>
                                    <th>Nama Dokter</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Alamat</th>
                                    <th>No Telp</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>


                            <tbody>
                                <?php $no = 1; ?>
                                <?php foreach($dokters as $dokter){ ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $dokter['nama_dokter'] ?></td>
                                    <td><?= $dokter['jenkel'] ?></td>
                                    <td><?= $dokter['alamat'] ?></td>
                                    <td><?= $dokter['no_tlp'] ?></td>
                                    <td>
                                        <a class="btn btn-danger btn-sm text-white" type="button"
                                            href="/dokter/delete/<?= $dokter['id_dokter'];?>"
                                            onclick="return confirm('Anda yakin mau menghapus data ini ?')">Delete</a>
                                        <button class="btn btn-warning btn-sm" data-toggle="modal"
                                            data-target="#edit-modal" id="edit" data-id="<?=$dokter['id_dokter']?>"
                                            data-nama_dokter="<?=$dokter['nama_dokter']?>"
                                            data-jenkel="<?=$dokter['jenkel']?>" data-alamat="<?=$dokter['alamat']?>"
                                            data-tlp="<?=$dokter['no_tlp']?>" data-jam="<?=$dokter['jam_kerja']?>" data-ket="<?=$dokter['keterangan']?>">Edit</button>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div> <!-- End Row -->

<!-- Tambah modal content -->
<div id="custom-width-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title m-0" id="custom-width-modalLabel">Tambah
                    Dokter</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="<?=base_url('/dokter/insert')?>" method="post">
                    <?= csrf_field(); ?>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 control-label">Nama Dokter</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" id="inputEmail3" placeholder="Nama Dokter"
                                name="nama_dokter">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 control-label">Jenis Kelamin</label>
                        <div class="col-sm-9">
                            <!-- <input type="" class="form-control" id="inputPassword3" placeholder="Jenis Kelamin" name="jenkel"> -->
                            <select class="form-control" name="jenkel">
                                <option value="Laki-Laki">Laki-Laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Alamat</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" id="inputPassword4" placeholder="Alamat" name="alamat">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">No Telp</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" id="inputPassword4" placeholder="No Telp" name="no_tlp">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 control-label">Jam Praktik (dari)</label>
                        <div class="col-sm-9">
                            <input id="timepicker2" type="text" class="form-control" name="jam_mulai">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 control-label">Jam Praktik (sampai)</label>
                        <div class="col-sm-9">
                            <input id="timepicker4" type="text" class="form-control" name="jam_akhir">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Keterangan</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" id="inputPassword4" placeholder="Keterangan" name="keterangan">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Edit modal content -->
<div id="edit-modal" name="edit-modal" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="custom-width-modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title m-0" id="custom-width-modalLabel">Edit Dokter</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form_id" action="<?=base_url('/dokter/edit')?>" method="post">
                    <?= csrf_field(); ?>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 control-label">Nama Dokter</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" id="nama_dokter" placeholder="Nama Dokter"
                                name="nama_dokter">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 control-label">Jenis
                            Kelamin</label>
                        <div class="col-sm-9">
                            <!-- <input type="" class="form-control" id="inputPassword3" placeholder="Jenis Kelamin" name="jenkel"> -->
                            <select class="form-control" name="jenkel">
                                <option selected id="jenkel"></option>
                                <option value="Laki-Laki">Laki-Laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Alamat</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" id="alamat" placeholder="Alamat" name="alamat">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">No Telp</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" id="no_tlp" placeholder="No Telp" name="no_tlp">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 control-label">Jam Praktik (dari)</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="jam_mulai" id="jam_mulai">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 control-label">Jam Praktik (sampai)</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="jam_akhir" id="jam_akhir">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Keterangan</label>
                        <div class="col-sm-9">
                            <input type="" id="keterangan" name="keterangan" class="form-control">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
// console.log(base_url + "pasien/edit/")
$(document).on("click", "#edit", function() {
    let id = $(this).data('id');
    let nama_dokter = $(this).data('nama_dokter');
    let jenkel = $(this).data('jenkel');
    let alamat = $(this).data('alamat');
    let tlp = $(this).data('tlp');
    let jam = $(this).data('jam');
    let keterangan = $(this).data('ket');
    let split = jam.split("-");
    let split1 = split[0];
    let split2 = split[1];
    let url = base_url + "dokter/edit/" + id;

    $("#id_dokter").val(id);
    $("#nama_dokter").val(nama_dokter);
    $("#jenkel").val(jenkel);
    $("#jenkel").text(jenkel);
    $("#alamat").val(alamat);
    $("#no_tlp").val(tlp);
    $("#jam_mulai").val(split1);
    $("#jam_akhir").val(split2);
    $("#keterangan").val(keterangan);
    $("#form_id").attr("action", url);
})
</script>
<?= $this->endSection() ?>