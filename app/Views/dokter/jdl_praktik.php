<?= $this->extend('layout') ?>
<?= $this->section('content') ?>
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title m-0">Datatables</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="m-b-30 m-t-0">Jadwal Praktik Dokter</h4>
                <button <?=$status?> type="button" class="btn btn-success waves-effect waves-light" data-toggle="modal"
                    data-target="#custom-width-modal">Tambah Jadwal Dokter</button>
                <?php
                        if (session()->getFlashData('success')) {
                        ?>
                <div class="alert alert-success alert-dismissible fade show">
                    <?= session()->getFlashData('success') ?>
                </div>
                <?php } ?>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
                            style="border-collapse: collapse; width: 100%;">
                            <thead>
                                <tr>
                                    <th>No. </th>
                                    <th>Nama Dokter</th>
                                    <th>Jam Praktik</th>
                                    <th>Keterangan</th>
                                    <th <?= hak_akses() ?>>Aksi</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php $no = 1; ?>
                                <?php foreach($jadwal as $j) {?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $j['nama_dokter'] ?></td>
                                    <td><?= $j['jam_kerja'] ?></td>
                                    <td><?= $j['keterangan'] ?></td>
                                    <td <?= hak_akses() ?>>
                                        <a class="btn btn-danger btn-sm text-white" type="button"
                                            href="/jdl_praktik/delete/<?= $j['id_jdlpraktik'];?>"
                                            onclick="return confirm('Anda yakin mau menghapus data ini ?')">Delete</a>
                                        <a class="btn btn-warning btn-sm" data-toggle="modal" data-target="#edit-modal"
                                            id="edit_jdlpraktik" data-id="<?=$j['id_jdlpraktik']?>"
                                            data-nama_dokter="<?=$j['nama_dokter']?>" data-jam="<?=$j['jam_kerja']?>"
                                            data-ket="<?=$j['keterangan']?>">Edit</a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div> <!-- End Row -->

<!-- Insert Modal Praktik Kerja Dokter -->
<div id="custom-width-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title m-0" id="custom-width-modalLabel">Jam Kerja Dokter</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="<?=base_url('/jdl_praktik/insert')?>" method="post">
                    <?= csrf_field(); ?>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 control-label">Nama Dokter</label>
                        <div class="col-sm-9">
                            <select type="text" name="nama_dokter" class=" form-control" id="inputEmail3"
                                placeholder="Nama Dokter">
                                <?php foreach($dokter as $dokter){ ?>
                                <option value="<?= $dokter['nama_dokter']; ?>">
                                    <?= $dokter['nama_dokter']; ?> </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 control-label">Jam Praktik (dari)</label>
                        <div class="col-sm-9">
                            <input id="timepicker2" type="text" class="form-control" name="jam_mulai">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 control-label">Jam Praktik (sampai)</label>
                        <div class="col-sm-9">
                            <input id="timepicker4" type="text" class="form-control" name="jam_akhir">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Keterangan</label>
                        <div class="col-sm-9">
                            <input type="text" name="keterangan" class="form-control" id="inputPassword4"
                                placeholder="Keterangan">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                    </div>
                </form>
            </div><!-- End Modal Body -->
        </div><!-- End Modal content -->
    </div><!-- End Modal Dialog -->
</div>
<!--End Insert Modal Praktik Kerja Dokter  -->

<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title m-0" id="custom-width-modalLabel">Jam Kerja Dokter</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="" method="post" id="form_id">
                    <?= csrf_field(); ?>
                    <!-- <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 control-label">Nama Dokter</label>
                        <div class="col-sm-9">
                            <select type="text" name="nama_dokter" class=" form-control">                                                        
                                <option selected id="nama_dokter"></option>
                            </select>
                        </div>
                    </div> -->
                    <input type="text" id="id_dokter" hidden>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 control-label">Jam Praktik (dari)</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="jam_mulai" id="jam_mulai">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 control-label">Jam Praktik (sampai)</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="jam_akhir" id="jam_akhir">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Keterangan</label>
                        <div class="col-sm-9">
                            <input type="" id="keterangan" name="keterangan" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                    </div>
                </form>
            </div><!-- End Modal Body -->
        </div><!-- End Modal content -->
    </div><!-- End Modal Dialog -->
</div>


<script>
// console.log(base_url + "pasien/edit/")
$(document).on("click", "#edit_jdlpraktik", function() {
    let id = $(this).data('id');
    let nama_dokter = $(this).data('nama_dokter');
    let jam = $(this).data('jam');
    let keterangan = $(this).data('ket');
    let url = base_url + "jdl_praktik/edit/" + id;
    let split = jam.split("-");
    let split1 = split[0];
    let split2 = split[1];

    // console.log(array);
    $("#id_dokter").val(id);
    $("#nama_dokter").val(nama_dokter);
    $("#nama_dokter").text(nama_dokter);
    $("#jam_mulai").val(split1);
    $("#jam_akhir").val(split2);
    $("#keterangan").val(keterangan);
    $("#form_id").attr("action", url);
})
</script>






<?= $this->endSection() ?>