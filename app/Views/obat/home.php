<?= $this->extend('layout') ?>
<?= $this->section('content') ?>
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title m-0">Datatables</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="m-b-30 m-t-0">Default Example</h4>
                <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="modal"
                    data-target="#custom-width-modal">Tambah Obat</button>
                    <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="modal"
                    data-target="#kategori-obat">Tambah Jenis Obat</button>
                <?php
                        if (session()->getFlashData('success')) {
                        ?>
                <div class="alert alert-success alert-dismissible fade show">
                    <?= session()->getFlashData('success') ?>
                </div>
                <?php } ?>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
                            style="border-collapse: collapse; width: 100%;">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Obat</th>
                                    <th>Jenis Obat</th>
                                    <th>Stok Obat</th>
                                    <th>Harga Obat</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>


                            <tbody>
                                <?php $no = 1; ?>
                                <?php foreach($obats as $obat){ ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $obat['nama_obat'] ?></td>
                                    <td><?= $obat['jenis_obat'] ?></td>
                                    <td><?= $obat['stock_obat'] ?></td>
                                    <td><?= $obat['harga_obat'] ?></td>
                                    <td>
                                        <a class="btn btn-danger btn-sm text-white" type="button"
                                            href="/obat/delete/<?= $obat['id_obat'];?>"
                                            onclick="return confirm('Anda yakin mau menghapus data ini ?')">Delete</a>
                                        <button class="btn btn-warning btn-sm" data-toggle="modal"
                                            data-target="#edit-modal" id="edit" data-id="<?=$obat['id_obat']?>"
                                            data-nama_obat="<?=$obat['nama_obat']?>"
                                            data-jenis_obat="<?=$obat['jenis_obat']?>"
                                            data-stock_obat="<?=$obat['stock_obat']?>"
                                            data-harga_obat="<?=$obat['harga_obat']?>">Edit</button>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div> <!-- End Row -->

<!-- Tambah modal content -->
<div id="custom-width-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title m-0" id="custom-width-modalLabel">Form Obat</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="<?= base_url('/obat/insert-obat') ?>" method="post">
                    <?= csrf_field(); ?>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 control-label">Nama Obat</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" placeholder="Nama Obat" name="nama_obat">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 control-label">Jenis Obat</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="jenis_obat">
                                <?php foreach($kategori as $k){ ?>
                                <option value="<?= $k['id_jenis_obat'] ?>"><?= $k['jenis_obat'] ?></option>                                
                                <?php } ?>
                            </select>                            
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Stok Obat</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" placeholder="Stok Obat" name="stock_obat">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Harga Obat</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" placeholder="Harga Obat" name="harga_obat">
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Edit modal content -->
<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title m-0" id="custom-width-modalLabel">Edit Obat</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form_id" action="<?= base_url('/obat/edit')?>" method="add">
                    <?= csrf_field(); ?>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 control-label">Nama Obat</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" id="nama_obat" name="nama_obat">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 control-label">Jenis Obat</label>
                        <div class="col-sm-9">
                        <?php foreach($obats as $o){ ?>
                                <select class="form-control" name="jenis_obat">
                                <option value="<?= $o['id_jenis_obat'] ?>"><?= $o['jenis_obat'] ?></option>                                
                            </select>                            
                            <?php } ?>
                            <!-- <input type="" class="form-control" id="jenis_obat" name="jenis_obat"> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Stok Obat</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="stock_obat" name="stock_obat">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Harga Obat</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" id="harga_obat" name="harga_obat">
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Tambah kategori obat modal content -->
<div id="kategori-obat" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title m-0" id="custom-width-modalLabel">Jenis Obat</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="" action="<?= base_url('/obat/insert_kategori_obat') ?>" method="post">
                    <?= csrf_field(); ?>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 control-label">Jenis Obat</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" id="kategori_obat" name="jenis_obat">
                        </div>
                    </div>                    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
// console.log(base_url + "pasien/edit/")
$(document).on("click", "#edit", function() {
    let id = $(this).data('id');
    let nama_obat = $(this).data('nama_obat');
    let jenis_obat = $(this).data('jenis_obat');
    let stock_obat = $(this).data('stock_obat');
    let harga_obat = $(this).data('harga_obat');
    let url = base_url + "obat/edit/" + id;

    $("#id_obat").val(id);
    $("#nama_obat").val(nama_obat);
    $("#jenis_obat").val(jenis_obat);
    $("#stock_obat").val(stock_obat);
    $("#harga_obat").val(harga_obat);
    $("#form_id").attr("action", url);
})
</script>
<?= $this->endSection() ?>