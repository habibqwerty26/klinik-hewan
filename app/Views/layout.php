<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>KLINIK HEWAN ABADI SUMBAWA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta content="Admin Dashboard" name="description" />
    <meta content="ThemeDesign" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="assets/images/kliniklogo.jpg">

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="assets/plugins/morris/morris.css">

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css">

    <!-- DataTables -->
    <link href="<?=base_url('assets/plugins/datatables/jquery.dataTables.min.css')?>" rel="stylesheet"
        type="text/css" />
    <link href="<?=base_url('assets/plugins/datatables/buttons.bootstrap4.min.css')?>" rel="stylesheet"
        type="text/css" />
    <link href="<?=base_url('assets/plugins/datatables/fixedHeader.bootstrap4.min.css')?>" rel="stylesheet"
        type="text/css" />
    <link href="<?=base_url('assets/plugins/datatables/responsive.bootstrap4.min.css')?>" rel="stylesheet"
        type="text/css" />
    <link href="<?=base_url('assets/plugins/datatables/dataTables.bootstrap4.min.css')?>" rel="stylesheet"
        type="text/css" />
    <link href="<?=base_url('assets/plugins/datatables/scroller.bootstrap4.min.css')?>" rel="stylesheet"
        type="text/css" />

    <link href="<?= base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/plugins/timepicker/bootstrap-timepicker.min2.css') ?>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>

    <script type="text/javascript">
    let base_url = '<?= base_url() ?>'
    </script>

</head>

<body>

    <div class="header-bg">
        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container-fluid">

                    <!-- Logo-->
                    <div>
                        <a href="" class="logo">
                            <p><img src="assets/images/kliniklogo.jpg" class="logo-lg" alt="" height="60"
                                    style=”float:left;” /></p>
                        </a>
                    </div>
                    <!-- End Logo-->

                    <div class="menu-extras topbar-custom navbar p-0">

                        <ul class="mb-0 nav navbar-right ml-auto list-inline">


                            <!-- <li class="list-inline-item notification-list d-none d-sm-inline-block">
                                <a href="#" id="btn-fullscreen" class="waves-effect waves-light notification-icon-box"><i class="fas fa-expand"></i></a>
                            </li> -->
                            <li class="dropdown">
                                <a href="" class="dropdown-toggle profile waves-effect waves-light"
                                    data-toggle="dropdown" aria-expanded="true">
                                    <img src="assets/images/users/avatar-1.jpg" alt="user-img" class="rounded-circle">
                                    <span class="profile-username">
                                        <?= session()->get('username'); ?> <span
                                            class="mdi mdi-chevron-down font-15"></span>
                                    </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- <li><a href="javascript:void(0)" class="dropdown-item"> Profile</a></li>
                                    <li><a href="javascript:void(0)" class="dropdown-item"><span
                                                class="badge badge-success float-right">5</span> Settings </a></li>
                                    <li><a href="javascript:void(0)" class="dropdown-item"> Lock screen</a></li>
                                    <li class="dropdown-divider"></li> -->
                                    <li onclick="return confirm('Apakah anda yakin ingin keluar ?')"><a
                                            href="<?= base_url('/adm/logout'); ?>" class="dropdown-item"> Logout</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="menu-item dropdown notification-list list-inline-item">
                                <!-- Mobile menu toggle-->
                                <a class="navbar-toggle nav-link">
                                    <div class="lines">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </a>
                                <!-- End mobile menu toggle-->
                            </li>

                        </ul>

                    </div>
                    <!-- end menu-extras -->

                    <div class="clearfix"></div>

                </div>
                <!-- end container -->
            </div>
            <!-- end topbar-main -->

            <!-- MENU Start -->
            <?= $this->include('dashboard') ?>
            <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->

    </div>
    <!-- header-bg -->

    <div class="wrapper">
        <div class="container-fluid">
            <!-- Page-Title -->
            <?= $this->renderSection('content') ?>

        </div>
        <!-- end container-fluid -->
    </div>
    <!-- end wrapper -->

    <!-- Footer -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © 2023 - 2024 Masehi <span class="d-none d-md-inline-block"> - Crafted with <i
                            class="mdi mdi-heart text-danger"></i> by Flockbase.id</span>
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

    <!-- jQuery  -->
    <script src="<?=base_url('assets/js/jquery.min.js')?>"></script>
    <script src="<?=base_url('assets/js/bootstrap.bundle.min.js')?>"></script>
    <script src="<?=base_url('assets/js/modernizr.min.js')?>"></script>
    <script src="<?=base_url('assets/js/detect.js')?>"></script>
    <script src="<?=base_url('assets/js/fastclick.js')?>"></script>
    <script src="<?=base_url('assets/js/jquery.slimscroll.js')?>"></script>
    <script src="<?=base_url('assets/js/jquery.blockUI.js')?>"></script>
    <script src="<?=base_url('assets/js/waves.js')?>"></script>
    <script src="<?=base_url('assets/js/wow.min.js')?>"></script>
    <script src="<?=base_url('assets/js/jquery.nicescroll.js')?>"></script>
    <script src="<?=base_url('assets/js/jquery.scrollTo.min.js')?>"></script>

    <!-- KNOB JS -->
    <script src="<?=base_url('')?>assets/plugins/jquery-knob/excanvas.js"></script>
    <script src="<?=base_url('')?>assets/plugins/jquery-knob/jquery.knob.js"></script>

    <script src="<?=base_url('')?>assets/plugins/flot-chart/jquery.flot.min.js"></script>
    <script src="<?=base_url('')?>assets/plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
    <script src="<?=base_url('')?>assets/plugins/flot-chart/jquery.flot.resize.js"></script>
    <script src="<?=base_url('')?>assets/plugins/flot-chart/jquery.flot.pie.js"></script>
    <script src="<?=base_url('')?>assets/plugins/flot-chart/jquery.flot.selection.js"></script>
    <script src="<?=base_url('')?>assets/plugins/flot-chart/jquery.flot.stack.js"></script>
    <script src="<?=base_url('')?>assets/plugins/flot-chart/jquery.flot.crosshair.js"></script>

    <!-- Required datatable js-->
    <script src="<?=base_url('')?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=base_url('')?>assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="<?=base_url('')?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="<?=base_url('')?>assets/plugins/datatables/buttons.bootstrap4.min.js"></script>

    <script src="<?=base_url('assets/plugins/timepicker/bootstrap-timepicker.js')?>"></script>
    <script src="<?=base_url('assets/plugins/timepicker/bootstrap-timepicker2.js')?>"></script>
    <script src="<?=base_url('')?>assets/plugins/datatables/jszip.min.js"></script>
    <script src="<?=base_url('')?>assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="<?=base_url('')?>assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="<?=base_url('')?>assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="<?=base_url('')?>assets/plugins/datatables/buttons.print.min.js"></script>
    <script src="<?=base_url('')?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
    <script src="<?=base_url('')?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
    <script src="<?=base_url('')?>assets/plugins/datatables/dataTables.scroller.min.js"></script>


    <!-- Responsive examples -->
    <script src="<?=base_url('')?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="<?=base_url('')?>assets/plugins/datatables/responsive.bootstrap4.min.js"></script>

    <!-- Datatable init js -->
    <script src="<?=base_url('')?>assets/pages/datatables.init.js"></script>
    <script src="<?=base_url('')?>assets/pages/form-advanced.js"></script>


    <script src="<?=base_url('')?>assets/pages/dashboard.js"></script>

    <script src="<?=base_url('')?>assets/js/app.js"></script>

</body>

</html>