<div class="navbar-custom">
    <div class="container-fluid">

        <div id="navigation">

            <!-- Navigation Menu-->
            <ul class="navigation-menu">

                <li class="has-submenu">
                    <a href="<?=base_url('adm')?>"><i class="ti-home"></i> Beranda</a>
                </li>

                <li class="has-submenu" <?= hak_akses() ?>>
                    <a href="<?=base_url('dokter')?>"><i class="ti-briefcase"></i> Dokter <i></i></a>
                </li>

                <li class="has-submenu" <?= hak_akses() ?>>
                    <a href="<?=base_url('pasien')?>"><i class="ti-harddrives"></i> Pasien <i></i></a>
                </li>

                <li class="has-submenu" <?= hak_akses()?>>
                    <a href="<?=base_url('obat')?>"><i class="ti-files"></i> Obat <i></i></a>
                </li>

                <!-- <li class="has-submenu">
                    <a href="<?=base_url('jdl_praktik')?>"><i class="ti-agenda"></i> Jadwal Praktik <i></i></a>
                </li> -->

                <li class="has-submenu" <?= hak_user()?>>
                    <a href="<?=base_url('pemeriksaan')?>"><i class="ti-agenda"></i> Hasil Pemeriksaan <i></i></a>
                </li>
                
                <li class="has-submenu" <?= hak_akses()?>>
                    <a href="<?=base_url('diagnosa')?>"><i class="ti-files"></i> Diagnosa <i></i></a>
                </li>

            </ul>
            <!-- End navigation menu -->
        </div>
        <!-- end #navigation -->
    </div>
    <!-- end container -->
</div>