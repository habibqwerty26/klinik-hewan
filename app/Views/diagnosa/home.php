<?php $this->extend('layout') ?>
<?php $this->section('content') ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="m-b-30 m-t-0">Diagnosa</h4>                
                <div class="row">
                <?php
                        if (session()->getFlashData('success')) {
                        ?>
                <div class="alert alert-success alert-dismissible fade show">
                    <?= session()->getFlashData('success') ?>
                </div>
                <?php } ?>
                    <div class="col-lg-12 col-sm-12 col-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
                            style="border-collapse: collapse; width: 100%;">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Pasien</th>
                                    <th>Nama Hewan</th>
                                    <th>Spesies</th>
                                    <th>Keluhan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>                            
                            <tbody>
                                <?php $no=1; ?>
                                <?php foreach($diagnosa as $diagnosa){ ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><?= $diagnosa['nama_pemilik'] ?></td>
                                    <td><?= $diagnosa['nama_hewan'] ?></td>
                                    <td><?= $diagnosa['spesies'] ?></td>
                                    <td><?= $diagnosa['keluhan'] ?></td>
                                    <td>
                                        <a class="btn btn-warning btn-sm" data-target="#custom-width-modal" data-toggle="modal" data-id="<?=$diagnosa['id_pasien']?>" data-id_periksa="<?=$diagnosa['id_periksa']?>" data-nama="<?=$diagnosa['nama_pemilik']?>" data-spesies="<?=$diagnosa['spesies']?>" data-umur="<?=$diagnosa['umur']?>" data-keluhan="<?=$diagnosa['keluhan']?>" id="periksa">Periksa</a>
                                        <a class="btn btn-danger btn-sm text-white" href="">Delete</a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div> <!-- End Row -->

<!-- Tambah modal content -->
<div id="custom-width-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title m-0" id="custom-width-modalLabel">Form Periksa</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="<?= base_url() ?>" method="post" id="form_id">
                    <?= csrf_field(); ?>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 control-label">Nama pasien</label>
                        <div class="col-sm-9">
                            <!-- <input type="" name="id_periksa" id="id_periksa" hidden> -->
                            <input type="" class="form-control" placeholder="" name="nama_pasien" id="nama_pasien" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 control-label">Spesies</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" placeholder="" name="spesies" id="spesies" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Umur</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" placeholder="" name="umur" id="umur" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Keluhan</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" placeholder="" name="keluhan" id="keluhan" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Diagnosa</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" placeholder="" name="diagnosa">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Jenis Obat</label>
                        <div class="col-sm-9">
                        <select class="form-control" name="jenis_obat" id="jenis_obat">
                                <?php foreach($kategori_obat as $k){ ?>
                                <option value="<?= $k['id_jenis_obat'] ?>"><?= $k['jenis_obat'] ?></option>                                
                                <?php } ?>
                            </select>                              
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Nama Obat</label>
                        <div class="col-sm-9">
                            <select name="nama_obat" class="form-control" id="nama_obat">

                            </select>                            
                        </div>
                    </div>                    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    $(document).ready(function(){
        $("#jenis_obat").change(function(){
            var jenis_obat = $(this).val()
            // console.log(jenis_obat);
            $.ajax({
                url: base_url + "/obat/kategori_obat/" + jenis_obat,
                method: "POST",
                data:{jenis_obat: jenis_obat},
                success: function(data){
                    $("#nama_obat").html(data);                    
                }
            })
        });
    });

    $(document).on("click", "#periksa", function() {
        let id = $(this).data('id_periksa');
        let nama_pasien = $(this).data('nama');
        let spesies = $(this).data('spesies');
        let umur = $(this).data('umur');
        let keluhan = $(this).data('keluhan');
        let url = base_url + "diagnosa/periksa_pasien/" + id;
        
        $("#nama_pasien").val(nama_pasien);
        $("#spesies").val(spesies);
        $("#umur").val(umur);
        $("#keluhan").val(keluhan);
        $("#form_id").attr("action", url);
    });
</script>

<?php $this->endSection('layout') ?>