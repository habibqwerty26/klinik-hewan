<?= $this->extend('user/layout') ?>
<?= $this->section('content') ?>
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title m-0">Dashboard</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="row">
                <div class="col-sm-6 col-xl-3">
                    <div class="card">
                        <div class="card-heading p-4">
                            <div>
                                <input class="knob" data-width="80" data-height="80" data-linecap=round data-fgColor="#fd8442" value="78" data-skin="tron" data-angleOffset="180" data-readOnly=true data-thickness=".15" />
                                <div class="float-right">
                                    <h2 class="text-primary mb-0">8952</h2>
                                    <p class="text-muted mb-0 mt-2">Total Sales</p>
                                </div>
                                <p class="mt-4 mb-0 text-muted"><b>78% </b>From Last 24 Hours <span class="float-right"><i class="fa fa-caret-up m-r-5"></i>10.25%</span></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-xl-3">
                    <div class="card">
                        <div class="card-heading p-4">
                            <div>
                                <input class="knob" data-width="80" data-height="80" data-linecap=round data-fgColor="#bb96ea" value="62" data-skin="tron" data-angleOffset="180" data-readOnly=true data-thickness=".15" />
                                <div class="float-right">
                                    <h2 class="text-purple mb-0">6521</h2>
                                    <p class="text-muted mb-0 mt-2">New Orders</p>
                                </div>
                                <p class="mt-4 mb-0 text-muted"><b>62% </b>Orders Last 10 months<span class="float-right"><i class="fa fa-caret-up m-r-5"></i>10.25%</span></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-xl-3">
                    <div class="card">
                        <div class="card-heading p-4">
                            <div>
                                <input class="knob" data-width="80" data-height="80" data-linecap=round data-fgColor="#fd8442" value="42" data-skin="tron" data-angleOffset="180" data-readOnly=true data-thickness=".15" />
                                <div class="float-right">
                                    <h2 class="text-primary mb-0">4526</h2>
                                    <p class="text-muted mb-0 mt-2">New Users</p>
                                </div>
                                <p class="mt-4 mb-0 text-muted"><b>42% </b>From Last 24 Hours <span class="float-right"><i class="fa fa-caret-up m-r-5"></i>10.25%</span></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-xl-3">
                    <div class="card">
                        <div class="card-heading p-4">
                            <div>
                                <input class="knob" data-width="80" data-height="80" data-linecap=round data-fgColor="#bb96ea" value="25" data-skin="tron" data-angleOffset="180" data-readOnly=true data-thickness=".15" />
                                <div class="float-right">
                                    <h2 class="text-purple mb-0">5621</h2>
                                    <p class="text-muted mb-0 mt-2">Unique Visitors</p>
                                </div>
                                <p class="mt-4 mb-0 text-muted"><b>25% </b>From Last 1 Month <span class="float-right"><i class="fa fa-caret-up m-r-5"></i>10.25%</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <h4 class="m-t-0">Visi & Misi</h4>

                <div style="height: 300px"></div>
            </div>
        </div>
    </div>



</div> <!-- end container-fluid -->



<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="m-b-30 m-t-0">Jadwal Praktik Dokter</h4>
                <!-- <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="modal"
                    data-target="#custom-width-modal">Tambah Jadwal Dokter</button> -->
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
                            style="border-collapse: collapse; width: 100%;">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Dokter</th>
                                    <th>Jam Praktik</th>
                                    <th>Keterangan</th>
                                    <!-- <th>Aksi</th> -->
                                </tr>
                            </thead>
                            <!-- ['nama_dokter', 'jam_kerja', 'keterangan']; -->
                            <tbody>
                                <?php $no = 1; ?>
                                <?php foreach ($jadwal as $j) { ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $j['nama_dokter']; ?></td>
                                    <td><?= $j['jam_kerja']; ?></td>
                                    <td><?= $j['keterangan']; ?></td>
                                    <td>
                                        <!-- <a class="btn btn-danger btn-sm text-white" href="">Delete</a>
                                <a class="btn btn-warning btn-sm" href="">Edit</a> -->
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div> <!-- End Row -->


<div class="col-xl-3 col-lg-4 col-md-6 m-b-30">
    <button type="button" class="btn btn-success waves-effect waves-light">Pendaftaran</button>
    <!-- <button type="button" class="btn btn-success waves-effect waves-light">Pemeriksaan</button>                                         -->
</div>

<!-- <div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="m-b-30 m-t-0">Default Example</h4>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
                            style="border-collapse: collapse; width: 100%;">
                            <thead>
                                <tr>
                                    <th>Nama Hewan</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Umur</th>
                                    <th>Spesies</th>
                                    <th>Diagnosa</th>
                                    <th>Jenis Obat Yang Ditebus</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>


                            <tbody>
                                <tr>
                                    <td>Tiger Nixon</td>
                                    <td>System Architect</td>
                                    <td>Edinburgh</td>
                                    <td>61</td>
                                    <td>2011/04/25</td>
                                    <td>$320,800</td>
                                    <td>$320,800</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div> End Row -->

<?= $this->endSection() ?>