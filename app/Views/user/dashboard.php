<div class="navbar-custom">
    <div class="container-fluid">

        <div id="navigation">

            <!-- Navigation Menu-->
            <ul class="navigation-menu">

                <li class="has-submenu">
                    <a href="<?=base_url('user')?>"><i class="ti-home"></i> Beranda</a>
                </li>

                <li class="has-submenu">
                    <a href="<?=base_url('dokter')?>"><i class="ti-briefcase"></i> Dokter <i></i></a>
                </li>
            </ul>
            <!-- End navigation menu -->
        </div>
        <!-- end #navigation -->
    </div>
    <!-- end container -->
</div>