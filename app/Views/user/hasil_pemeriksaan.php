<?= $this->extend('layout') ?>
<?= $this->section('content') ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="m-b-30 m-t-0">Hasil Pemeriksaan</h4>
                <?php
                        if (session()->getFlashData('success')) {
                        ?>
                <div class="alert alert-success alert-dismissible fade show">
                    <?= session()->getFlashData('success') ?>
                </div>
                <?php } ?>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
                            style="border-collapse: collapse; width: 100%;">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama Dokter</th>
                                    <th>Spesies</th>
                                    <th>Umur</th>
                                    <th>Diagnosa</th>
                                    <th>Obat</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php $no = 1; ?>
                                <?php foreach($hewan as $hewan){ ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><?= $hewan['nama_dokter'] ?></td>
                                    <td><?= $hewan['spesies'] ?></td>
                                    <td><?= $hewan['umur'] ?></td>
                                    <td><?= $hewan['diagnosa']?></td>                                        
                                    <td><?=$hewan['nama_obat']?></td>
                                    <td>
                                        <a href="/pemeriksaan/cetak_pasien/<?=$hewan['id_periksa']?>" class="btn btn-primary waves-effect waves-light">Print</button>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div> <!-- End Row -->

<?= $this->endSection() ?>