<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>KLINIK HEWAN ABADI SUMBAWA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta content="Admin Dashboard" name="description" />
    <meta content="ThemeDesign" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="assets/images/kliniklogo.jpg">

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css">

</head>


<body>

    <!-- Begin page -->
    <div class="accountbg"></div>
    <div class="wrapper-page">
        <div class="card card-pages">

            <div class="card-body">
                <center>
                    <img src="assets/images/kliniklogo.jpg" width="250">
                    <h4>Klinik Hewan Abadi</h4>
                </center>
                <div class="text-center m-t-20 m-b-30">
                </div>
                <h4 class="text-muted text-center m-t-0"><b>Sign In</b></h4>
                <?php
                        if (session()->getFlashData('success')) {
                        ?>
                <div class="alert alert-success alert-dismissible fade show">
                    <?= session()->getFlashData('success') ?>
                </div>
                <?php } ?>

                <form class="form-horizontal m-t-20" action="<?=base_url('/login')?>" method="post">
                    <?= csrf_field(); ?>
                    <?php
                        if (session()->getFlashData('error')) {
                        ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <?= session()->getFlashData('error') ?>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                        <div class="col-12">
                            <input class="form-control" type="text" required="" name="username" placeholder="Username">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-12">
                            <input class="form-control" type="password" required="" name="password"
                                placeholder="Password">
                        </div>
                    </div>


                    <div class="form-group text-center m-t-40">
                        <div class="col-12">
                            <button class="btn btn-primary btn-block btn-lg waves-effect waves-light" type="submit">Log
                                In</button>
                            <!-- <a href="base_url('adm')?>" class="btn btn-primary btn-block btn-lg waves-effect waves-light">Log In</a> -->
                        </div>
                    </div>
                </form>
                <div class="form-group row m-t-30 m-b-0">
                    <div class="col-sm-5 text-right">
                        <a href="#" class="text-muted" data-toggle="modal" data-target="#custom-width-modal">Buat
                            Akun Baru</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- sample modal content -->
    <div id="custom-width-modal" class="modal fade" tabindex="-1" role="dialog"
        aria-labelledby="custom-width-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title m-0" id="custom-width-modalLabel">Form Pembuatan Akun</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="<?=base_url('pasien/insert')?>" method="post">
                        <?= csrf_field(); ?>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 control-label">Nama Pemilik</label>
                            <div class="col-sm-9">
                                <input type="" class="form-control" name="namaPemilik" placeholder="Nama Pemilik">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-3 control-label">Jenis Kelamin</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="jenisKelamin" placeholder="Jenis Kelamin">
                                    <option value="Laki-Laki">Laki-laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword4" class="col-sm-3 control-label">Alamat</label>
                            <div class="col-sm-9">
                                <input type="" class="form-control" name="alamat" placeholder="Alamat">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword4" class="col-sm-3 control-label">No Telepon</label>
                            <div class="col-sm-9">
                                <input type="" class="form-control" name="noTelp" placeholder="No Telepon">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword4" class="col-sm-3 control-label">Username</label>
                            <div class="col-sm-9">
                                <input type="" class="form-control" id="username" name="username"
                                    placeholder="username">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword4" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" id="password" name="password"
                                    placeholder="Password">
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/modernizr.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <script src="assets/js/app.js"></script>

</body>

</html>