<?= $this->extend('layout') ?>
<?= $this->section('content') ?>
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title m-0">Datatables</h4>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="m-b-30 m-t-0">Daftar Register Pasien</h4>
                <?php
                        if (session()->getFlashData('success')) {
                        ?>
                <div class="alert alert-success alert-dismissible fade show">
                    <?= session()->getFlashData('success') ?>
                </div>
                <?php } ?>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-12">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
                            style="border-collapse: collapse; width: 100%;">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Pemilik</th>
                                    <th>Jenis Kelamin Hewan</th>
                                    <th>Alamat</th>
                                    <th>No Telepon</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>

                            <!-- ['nama_pemilik','jenkel','alamt','no_tlp']; -->
                            <tbody>
                                <?php $no = 1; ?>
                                <?php foreach ($pasien as $p) { ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $p['nama_pemilik']; ?></td>
                                    <td><?= $p['jenkel']; ?></td>
                                    <td><?= $p['alamat']; ?></td>
                                    <td><?= $p['no_tlp']; ?></td>
                                    <td>
                                        <a class="btn btn-danger btn-sm text-white" type="button"
                                            href="/pasien/delete/<?= $p['id_pasien'];?>"
                                            onclick="return confirm('Anda yakin mau menghapus data ini ?')">Delete</a>
                                        <!-- <a class="btn btn-warning btn-sm" href="">Edit</a> -->
                                        <a class="btn btn-warning btn-sm" data-toggle="modal" data-target="#edit-modal"
                                            id="edit" data-id="<?=$p['id_pasien']?>"
                                            data-nama_pemilik="<?=$p['nama_pemilik']?>" data-jenkel="<?=$p['jenkel']?>"
                                            data-alamat="<?=$p['alamat']?>" data-tlp="<?=$p['no_tlp']?>">Edit</a>
                                        <!-- <button type="button" class="btn btn-success btn-sm" data-toggle="modal"
                                        data-target="#custom-width-modal2" id="tambah_hewan"
                                        data-id_pasien="">Tambah Hewan</button> -->
                                        <!-- <a class="btn btn-warning btn-sm" href="">+Hewan</a> -->
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div> <!-- End Row -->

<div id="edit-modal" name="modal-edit" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="custom-width-modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title m-0" id="custom-width-modalLabel">Form Edit Pasien</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form_id" action="" method="post">
                    <?= csrf_field(); ?>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">PEMILIK</label>
                    </div>
                    <input hidden type="" class="form-control" id="id_pasien">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 control-label">Nama Pemilik</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" name="namaPemilik" id="nama_pemilik"
                                placeholder="Nama Pemilik">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 control-label">Jenis Kelamin</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="jenisKelamin" placeholder="Jenis Kelamin">
                                <option selected id="jenkel"></option>
                                <option value="Laki-Laki">Laki-laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Alamat</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" id="alamat" name="alamat" placeholder="Alamat">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">No Telepon</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" id="no_tlp" name="noTelp" placeholder="No Telepon">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Username</label>
                        <div class="col-sm-9">
                            <input type="" class="form-control" id="username" name="username" placeholder="username">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword4" class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" id="password" name="password"
                                placeholder="Password">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- <div id="custom-width-modal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title m-0" id="custom-width-modalLabel">Form Pasien</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" id="form_hewan" action="" method="post">
                                <?= csrf_field(); ?>
                                <div class="form-group row">
                                    <label for="inputPassword4" class="col-sm-3 control-label">HEWAN</label>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword4" class="col-sm-3 control-label">Nama Hewan</label>
                                    <div class="col-sm-9">
                                        <input type="" class="form-control" name="namaHewan" placeholder="Nama Hewan">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword4" class="col-sm-3 control-label">Jenis Kelamin</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="jenisKelamin" placeholder="Jenis Kelamin">
                                            <option value="Jantan">Jantan</option>
                                            <option value="Betina">Betina</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword4" class="col-sm-3 control-label">Spesies</label>
                                    <div class="col-sm-9">
                                        <input type="" class="form-control" name="spesies" placeholder="Spesies">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword4" class="col-sm-3 control-label">Umur</label>
                                    <div class="col-sm-9">
                                        <input type="" class="form-control" name="umur" placeholder="Umur">
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div> -->

<script>
// console.log(base_url + "pasien/edit/")
$(document).on("click", "#edit", function() {
    let id = $(this).data('id');
    let nama_pemilik = $(this).data('nama_pemilik');
    let jenkel = $(this).data('jenkel');
    let alamat = $(this).data('alamat');
    let tlp = $(this).data('tlp');
    let url = base_url + "pasien/edit/" + id;

    $("#id_pasien").val(id);
    $("#nama_pemilik").val(nama_pemilik);
    $("#jenkel").val(jenkel);
    $("#jenkel").text(jenkel);
    $("#alamat").val(alamat);
    $("#no_tlp").val(tlp);
    $("#form_id").attr("action", url);
})

$(document).on("click", "#tambah_hewan", function() {
    // console.log('ini tombol hewan');
    let id_pasien = $(this).data('id_pasien');
    let url = base_url + "pasien/insert_hewan/" + id_pasien;
    // console.log(url);

    $("#form_hewan").attr("action", url);
})
</script>
<?= $this->endSection() ?>