<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Periksa extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_periksa' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true
            ],
            'id_pasien' => [
                'type' => 'INT',
                'constraint' => 11
            ],
            'id_hewan' => [
                'type' => 'VARCHAR',
                'constraint' => 200
            ],
            'id_dokter' => [
                'type' => 'INT',
                'constraint' => 11
            ],
            'keluhan' => [
                'type' => 'VARCHAR',
                'constraint' => 200
            ],
            'diagnosa' =>[
                'type' => 'VARCHAR',
                'constraint' => 200
            ],
            'jenis_obat' =>[
                'type' => 'VARCHAR',
                'constraint' => 200
            ],
            'nama_obat' =>[
                'type' => 'VARCHAR',
                'constraint' => 200
            ],        
        ]);

        $this->forge->addKey('id_periksa', true);
        $this->forge->createTable('periksa');
    }

    public function down()
    {
        $this->forge->dropTable('periksa');
    }
}
