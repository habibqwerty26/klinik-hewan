<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Obat extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_obat' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true
            ],
            'id_jenis_obat' => [
                'type' => 'INT',
                'constraint' => 11
            ],
            'nama_obat' => [
                'type' => 'VARCHAR',
                'constraint' => 200
            ],
            'stock_obat' => [
                'type' => 'INT',
                'constraint' => 11
            ],
            'harga_obat' => [
                'type' => 'INT',
                'constraint' => 11
            ],
        ]);
        $this->forge->addKey('id_obat', true);
        $this->forge->createTable('obat');
    }

    public function down()
    {
        $this->forge->dropTable('obat');
    }
}
