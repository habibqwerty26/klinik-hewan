<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Dokter extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_dokter' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true
            ],
            'nama_dokter' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'jenkel' => [
                'type' => 'ENUM',
                'constraint' => ['Laki-Laki', 'Perempuan']
            ],
            'alamat' => [
                'type' => 'VARCHAR',
                'constraint' => 100
            ],
            'no_tlp' => [
                'type' => 'INT',
                'constraint' => 11
            ],
            'jam_kerja' => [
                'type' => 'VARCHAR',
                'constraint' => 200
            ],
            'keterangan' => [
                'type' => 'VARCHAR',
                'constraint' => 200,
                'null' => true
            ],
        ]);

        $this->forge->addKey('id_dokter', true);
        $this->forge->createTable('dokter');
    }

    public function down()
    {
        $this->forge->dropTable('dokter');
    }
}
