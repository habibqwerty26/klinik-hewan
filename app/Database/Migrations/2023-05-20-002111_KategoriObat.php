<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class KategoriObat extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_jenis_obat' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true
            ],
            'jenis_obat' => [
                'type' => 'VARCHAR',
                'constraint' => 200
            ],        
        ]);
        $this->forge->addKey('id_jenis_obat', true);
        $this->forge->createTable('kategori_obat');
    }

    public function down()
    {
        $this->forge->dropTable('kategori_obat');
    }
}
