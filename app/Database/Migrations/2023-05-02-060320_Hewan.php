<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Hewan extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_hewan' => [
                'type' => 'VARCHAR',
                'constraint' => 200,                
            ],
            'id_pasien' => [
                'type' => 'INT',
                'constraint' => 11
            ],
            'nama_hewan' => [
                'type' => 'VARCHAR',
                'constraint' => 200
            ],
            'jenkel_hewan' => [
                'type' => 'ENUM',
                'constraint' => ['Jantan', 'Betina']
            ],
            'spesies' => [
                'type' => 'VARCHAR',
                'constraint' => 100
            ],
            'umur' => [
                'type' => 'INT',
                'constraint' => 11
            ]        
        ]);

        $this->forge->addKey('id_hewan', true);
        $this->forge->createTable('hewan');
    }

    public function down()
    {
        $this->forge->dropTable('hewan');
    }
}
