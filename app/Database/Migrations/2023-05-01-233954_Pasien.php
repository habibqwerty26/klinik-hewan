<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Pasien extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_pasien' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true
            ],
            'nama_pemilik' => [
                'type' => 'VARCHAR',
                'constraint' => 100
            ],
            'jenkel' => [
                'type' => 'ENUM',
                'constraint' => ['Laki-Laki', 'Perempuan']
            ],
            'alamat' => [
                'type' => 'VARCHAR',
                'constraint' => 100
            ],
            'no_tlp' => [
                'type' => 'INT',
                'constraint' => 11
            ]
        ]);

        $this->forge->addKey('id_pasien', true);
        $this->forge->createTable('pasien');
    }

    public function down()
    {
        $this->forge->dropTable('pasien');
    }
}
