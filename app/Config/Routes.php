<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Login');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false);
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

$routes->get('/', 'Login::index');
$routes->post('/login', 'Login::login');
$routes->get('/adm/logout', 'Login::logout', ['as' => 'logout']);

$routes->group('', ['filter' => 'is_loggin'], function($routes){
    $routes->get('/adm', 'Dokter::index');
    $routes->get('/dokter', 'Dokter::dokter');
    $routes->post('/dokter/insert', 'Dokter::insertDokter');

    //Dokter
    $routes->get('/dokter', 'Dokter::dokter');
    $routes->post('/dokter/insert', 'Dokter::insertDokter');
    $routes->add('/dokter/edit/(:segment)', 'Dokter::editDokter/$1');
    $routes->get('/dokter/delete/(:num)', 'Dokter::deleteDokter/$1');

    //Pasien
    $routes->get('/pasien', 'Pasien::index');
    $routes->post('/pasien/insert_hewan/(:segment)', 'Pasien::insertHewan/$1');
    $routes->get('/pasien/delete/(:num)', 'Pasien::deletePasien/$1');
    $routes->add('/pasien/edit/(:segment)', 'Pasien::editPasien/$1');

    //Obat
    $routes->get('/obat', 'Obat::index');
    $routes->post('/obat/insert-obat', 'Obat::insertObat');
    $routes->post('/obat/insert_kategori_obat', 'Obat::insertJenisObat');
    $routes->add('/obat/edit/(:segment)', 'Obat::editObat/$1');
    $routes->get('/obat/delete/(:num)', 'Obat::deleteObat/$1');
    $routes->post('/obat/kategori_obat/(:num)', 'Obat::getCurrentObat/$1');

    //Jadwal Praktik
    $routes->get('/jdl_praktik', 'Dokter::jdl_praktik');
    $routes->post('/jdl_praktik/insert', 'Dokter::insertjdlPraktik');
    $routes->get('/jdl_praktik/delete/(:num)', 'Dokter::deletejdlPraktik/$1');
    $routes->add('/jdl_praktik/edit/(:segment)', 'Dokter::editjdlPrakrik/$1');

    //diagnosa
    $routes->get('/diagnosa', 'Dokter::diagnosa');
    $routes->add('/diagnosa/periksa_pasien/(:segment)', 'Dokter::periksa/$1');

    // User
    $routes->get('/user', 'User::index');
    $routes->get('/pemeriksaan', 'User::hPemeriksaan');
    $routes->get('/pemeriksaan/cetak_pasien/(:num)', 'Cetak::cetakPemeriksaan/$1');
});


// User
$routes->get('/user', 'User::index');
$routes->post('/pasien/insert', 'Pasien::insertPasien');
/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}