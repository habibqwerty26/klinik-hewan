<?php

namespace App\Controllers;
use App\Models\Obat as ObatModel;
use App\Models\KategoriObat;

class Obat extends BaseController
{
    private $obat_model;
    public function __construct()
    {
        $this->obat_model = new ObatModel();
        $this->kategori_obat_model = new KategoriObat();
    }

    public function index()
    {
        // $get_obat = $this->obat_model->findAll();
        // $get_kategori_obat = $this->kategori_obat_model->findAll();
        $get_kategori_obat = $this->kategori_obat_model->findAll();
        $get_obat = $this->obat_model->join('kategori_obat', 'obat.id_jenis_obat = kategori_obat.id_jenis_obat')->findAll();
        // var_dump($get_obat);
        return view('obat/home', ['obats' => $get_obat, 'kategori' => $get_kategori_obat]);
    }

    public function insertJenisObat()
    {
        $data = [
            'jenis_obat' => $this->request->getVar('jenis_obat'),
        ];
        // var_dump($data);
        $this->kategori_obat_model->insert($data);
        return redirect()->to('/obat')->with('success', 'data berhasil ditambahkan');
    }

    public function insertObat()
    {
        $data = [
            'nama_obat' => $this->request->getVar('nama_obat'),
            'id_jenis_obat' => $this->request->getVar('jenis_obat'),
            'stock_obat' => $this->request->getVar('stock_obat'),
            'harga_obat' => intVal($this->request->getVar('harga_obat'))
        ];
        // var_dump($data);
        $this->obat_model->insert($data);
        return redirect()->to('/obat')->with('success', 'data berhasil ditambahkan');
    }

    public function editObat ($id)
    {
        $this->obat_model->update($id, [
                'nama_obat' => $this->request->getVar('nama_obat'),
                // 'jenis_obat' => $this->request->getVar('jenis_obat'),
                'stock_obat' => $this->request->getVar('stock_obat'),
                'harga_obat' => intVal($this->request->getVar('harga_obat')),
            ]);

            return redirect()->to('/obat')->with('success', 'Data berhasil di update'); 
    }

    public function deleteObat ($id_obat)
    {
         $data = [
                'id_obat' => $id_obat
            ];
            $this->obat_model->delete($data);
		    return redirect()->to('/obat')->with('success', 'Data berhasil dihapuskan');
    }

    public function getCurrentObat($id_jenis_obat)
    {
        $output = '';
        $get_obat = $this->kategori_obat_model->join('obat', 'kategori_obat.id_jenis_obat = obat.id_jenis_obat')->where('obat.id_jenis_obat', $id_jenis_obat)->findAll();
        $get_obat2 = $this->obat_model->where('id_jenis_obat', $id_jenis_obat)->findAll();

        // dd($get_obat, $get_obat2);
        foreach($get_obat2 as $get){
            $output .= '<option value='.$get['nama_obat'].'>'.$get['nama_obat'].'</option>';
        }
        echo $output;
    }
}