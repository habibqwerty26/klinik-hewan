<?php

namespace App\Controllers;

use App\Models\Pasien as PasienModel;
use App\Models\Periksa;
use App\Models\Hewan;
use App\Models\Users;

class Pasien extends BaseController
{
    private $pasien_model;

    private $hewan_model;
    public function __construct()
    {
        $this->pasien_model = new PasienModel();
        $this->hewan_model =  new Hewan();
        $this->user_model = new Users();
        $this->periksa_model = new Periksa();
    }

    public function index()
    {
        // dd(session()->get('id_pasien'));
        $get_pasien = $this->pasien_model->findAll();
        $data = [
            'pasien' => $get_pasien
        ];
        return view('pasien/home', $data);
    }

    public function insertPasien()
    {
        $id = date('Ymd');
        $time = strval(time());
        $idfix = $id + $time;

        $data = [
            'id_pasien' => strval($idfix),  
            'nama_pemilik' => $this->request->getVar('namaPemilik'),
            'jenkel' => $this->request->getVar('jenisKelamin'),
            'alamat' => $this->request->getVar('alamat'),
            'no_tlp' => $this->request->getVar('noTelp')
        ];

        $data2 = [
            'id_pasien' => strval($idfix),
            'username' => $this->request->getVar('username'),
            'password' => $this->request->getVar('password'),
            'role' => 2
        ];
        // dd($data, $data2);
        $this->pasien_model->insert($data);
        $this->user_model->insert($data2);
        return redirect()->to('/')->with('success', 'Akun Telah Berhasil Dibuat');
    }

    public function editPasien($id)
    {
        
        $this->pasien_model->update($id, [
                'nama_pemilik' => $this->request->getPost('namaPemilik'),
                'jenkel' => $this->request->getPost('jenisKelamin'),
                'alamat' => $this->request->getPost('alamat'),
                'no_tlp' => $this->request->getPost('noTelp'),
            ]);

            return redirect()->to('/pasien')->with('success', 'data berhasil di update');
    }

    public function deletePasien ($id_pasien)
    {
         $data = [
                'id_pasien' => $id_pasien
            ];
            $this->pasien_model->delete($data);
		    return redirect()->to('/pasien')->with('success', 'Data berhasil dihapuskan');
    }

    public function insertHewan($id_pasien)
    {
        $id = date('Ymd');
        $time = strval(time());
        $id_hewan = 33 . $id + $time;
        $data_hewan = [
            'id_hewan' => intVal($id_hewan),
            'id_pasien' => $id_pasien,
            'nama_hewan' => $this->request->getVar('nama_hewan'),
            'jenkel' => $this->request->getVar('jenkel_hewan'),
            'spesies' => $this->request->getVar('spesies'),
            'umur' => $this->request->getVar('umur'),            
        ];

        $data_periksa = [
            'id_pasien' => $id_pasien,
            'id_hewan' => intVal($id_hewan),
            'id_dokter' => $this->request->getVar('id_dokter'),
            'keluhan' => $this->request->getVar('keluhan'),
        ];

        // dd($data_hewan, $data_periksa);
        $this->hewan_model->insert($data_hewan);
        $this->periksa_model->insert($data_periksa);
        return redirect()->to('/adm')->with('success', 'data hewan berhasil ditambahkan');
    }
}