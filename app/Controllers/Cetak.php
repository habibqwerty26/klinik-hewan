<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use Dompdf\Dompdf;
use App\Models\Periksa;

class Cetak extends BaseController
{
    private $periksa_model;
    private $dompdf;
    private $db;
    public function __construct()
    {
        // instantiate and use the dompdf class
        $this->dompdf = new Dompdf();
        $this->periksa_model = new Periksa();
        $this->db = db_connect();
    }
    public function cetakPemeriksaan($id_periksa)
    {
        $get_periksa = $this->db->query('SELECT * FROM periksa INNER JOIN pasien ON periksa.id_pasien = pasien.id_pasien 
                                    INNER JOIN dokter ON periksa.id_dokter = dokter.id_dokter
                                    INNER JOIN obat ON periksa.jenis_obat = obat.id_jenis_obat
                                    INNER JOIN hewan ON periksa.id_hewan = hewan.id_hewan
                                    WHERE id_periksa='. $id_periksa. '')->getRow();
        $id_jenis_obat = $get_periksa->id_jenis_obat;
        $get_kategori_obat = $this->db->query('SELECT * FROM kategori_obat WHERE id_jenis_obat='.$id_jenis_obat.'')->getRow();

        $filename = date('y-m-d-H-i-s'). '-qadr-labs-report';

        

        // load HTML content
        $this->dompdf->loadHtml(view('cetak/cetak_diagnosa', ['hasil' => $get_periksa, 'jenis_obat' => $get_kategori_obat]));

        // (optional) setup the paper size and orientation
        $this->dompdf->setPaper('A6', 'potrait');

        // render html as PDF
        $this->dompdf->render();

        // output the generated pdf
        $this->dompdf->stream($filename);
    }
}
