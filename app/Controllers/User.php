<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Dokter as DokterModel;
use App\Models\Hewan;
use App\Models\Periksa;

class User extends BaseController
{

    public function __construct()
    {
        $this->dokter_model = new DokterModel();
        $this->hewan_model = new Hewan();
        $this->periksa_model = new Periksa();
        
    }

    public function index()
    {
        return view('user/home');
    }

    public function hPemeriksaan()
    {
        $id_pasien = session()->get('id_pasien');
        $hewan = $this->periksa_model->join('dokter', 'periksa.id_dokter = dokter.id_dokter')
                                    ->join('hewan', 'periksa.id_hewan = hewan.id_hewan')
                                    ->join('pasien', 'periksa.id_pasien = pasien.id_pasien')
                                    ->where('periksa.id_pasien', $id_pasien)->findAll();
        // dd($hewan);
        return view('user/hasil_pemeriksaan', ['hewan' => $hewan]);
    }
}
