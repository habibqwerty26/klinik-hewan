<?php

namespace App\Controllers;
use App\Models\Users;

class Login extends BaseController
{
    public function register ()
    {
        # code...
    }
    public function login()
    {
        $users = new Users();
        $username = $this->request->getVar('username');
        $password = $this->request->getVar('password');

        $check_user = $users->where('username', $username)->first();
        // var_dump($check_user);
        if($check_user && $check_user['password'] === $password){
            if($check_user){
                session()->set([
                    'username' => $check_user['username'],
                    'role' => $check_user['role'],
                    'id_pasien' => $check_user['id_pasien'],
                    'logged_in' => true
                ]);
                return redirect()->to('/adm');
            }else{
                return redirect()->to('/')->with('error', 'username tidak ada atau password salah');
            }
        }else{
            return redirect()->to('/')->with('error', 'username tidak ada atau password salah');
        }
    }

    public function index()
    {
        return view('login');
    }
    
    public function logout ()
    {
         $session = session();
         $session->destroy();
        return redirect()->to('/');
    }

}