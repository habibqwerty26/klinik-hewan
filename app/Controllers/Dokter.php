<?php

namespace App\Controllers;
use App\Models\Dokter as DokterModel;
use App\Models\Hewan;
use App\Models\Periksa;
use App\Models\KategoriObat;

class Dokter extends BaseController
{
    public function __construct()
    {
        $this->dokter_model = new DokterModel();
        $this->hewan_model = new Hewan();
        $this->periksa_model = new Periksa();
        $this->kategori_obat_model = new KategoriObat();        
    }

// Tampilan View Halaman Data
//==============================================================================================================
    public function index()
    {
        $dokter = $this->dokter_model->findAll();
        // dd($dokter);
        $data = [
            'dokters' => $dokter
        ];
        return view('home', $data);
    }

    public function dokter()
    {
        $get_dokter = $this->dokter_model->findAll();
        // var_dump($get_dokter);
        $data = [
            'dokters' => $get_dokter
        ];
        return view('dokter/home', $data);
    }

      public function jdl_praktik()
    {
        $get_dokter = $this->dokter_model->findAll();
        $get_jadwal = $this->jadwal_model->findAll();
        $get_dokter = $this->dokter_model->findAll();
        $role = session()->get('role');

        if(intVal($role) == 2){
            $status = 'hidden';
        }else{
            $status ='';
        }
        
        $data = [
            'jadwal' => $get_jadwal,
            'dokter' => $get_dokter,
            'status' => $status
        ];
        
        return view('dokter/jdl_praktik', $data);
    }

    public function diagnosa()
    {        
        $diagnosa = $this->periksa_model->join('pasien', 'periksa.id_pasien = pasien.id_pasien')
                                        ->join('hewan', 'periksa.id_hewan = hewan.id_hewan')
                                        ->join('dokter', 'periksa.id_dokter = dokter.id_dokter')
                                        ->findAll();
        $get_kategori_obat = $this->kategori_obat_model->findAll();                                        
        $get_periksa = $this->periksa_model->where('id_pasien', )->findAll();
        return view('diagnosa/home', ['diagnosa' => $diagnosa, 'kategori_obat' => $get_kategori_obat]);
    }
// END Tampilan View Halaman Data

//Fungsi Tambah Ubah Hapus data
//============================================================================================================== 
    public function insertDokter()
    {       
        $mulai = $this->request->getVar('jam_mulai');
        $akhir = $this->request->getVar('jam_akhir');
        $jam = $mulai ."-". $akhir;
        $data = [
            'nama_dokter' => $this->request->getVar('nama_dokter'),
            'jenkel' => $this->request->getVar('jenkel'),
            'alamat' => $this->request->getVar('alamat'),
            'no_tlp' => intVal($this->request->getVar('no_tlp')),
            'jam_kerja' => $jam,
            'keterangan' => $this->request->getVar('keterangan'),
        ];
        // var_dump($data);
        $this->dokter_model->insert($data);
        return redirect()->to('/dokter')->with('success', 'data berhasil ditambahkan');
    }
      public function editDokter($id)
    {
        $mulai = $this->request->getVar('jam_mulai');
        $akhir = $this->request->getVar('jam_akhir');
        $jam = $mulai ."-". $akhir;   
        $this->dokter_model->update($id, [
                'nama_dokter' => $this->request->getPost('nama_dokter'),
                'jenkel' => $this->request->getPost('jenkel'),
                'alamat' => $this->request->getPost('alamat'),
                'no_tlp' => $this->request->getPost('no_tlp'),
                'jam_kerja' => $jam,
                'keterangan' => $this->request->getVar('keterangan')
            ]);

            return redirect()->to('/dokter')->with('success', 'Data berhasil di update');
    }

    public function deleteDokter ($id_dokter)
    {
         $data = [
                'id_dokter' => $id_dokter
            ];
            $this->dokter_model->delete($data);
		    return redirect()->to('/dokter')->with('success', 'Data berhasil dihapuskan');
    }
      
    
    
    public function insertjdlPraktik()
    {
        $mulai = $this->request->getVar('jam_mulai');
        $akhir = $this->request->getVar('jam_akhir');
        $jam = $mulai ."-". $akhir;

        $data = [
            'nama_dokter' => $this->request->getVar('nama_dokter'),
            'jam_kerja' => $jam,
            'keterangan' => $this->request->getVar('keterangan')
        ];
        // var_dump($data);
        $this->jadwal_model->insert($data);
        return redirect()->to('/jdl_praktik')->with('success', 'Data berhasil ditambahkan');
    }

         public function editjdlPrakrik ($id)
        {
        $mulai = $this->request->getVar('jam_mulai');
        $akhir = $this->request->getVar('jam_akhir');
        $jam = $mulai ."-". $akhir;
        // dd($jam);

        $this->jadwal_model->update($id, [
           'jam_kerja' => $jam,
            'keterangan' => $this->request->getVar('keterangan')
        ]);

        return redirect()->to('/jdl_praktik')->with('success', 'Data berhasil diubah');
        }
    
        public function deletejdlPraktik ($id_jdlpraktik)
        {
            $data = [
                'id_jdlpraktik' => $id_jdlpraktik
            ];
            $this->jadwal_model->delete($data);
		    return redirect()->to('/jdl_praktik')->with('success', 'Data berhasil dihapuskan');
        }
        
//END Fungsi Tambah Ubah Hapus data
        public function periksa($id)
        {                              
            // dd($id_periksa, $keluhan, $diagnosa, $jenis_obat, $nama_obat);
            $this->periksa_model->update($id, [       
                'diagnosa' => $this->request->getVar('diagnosa'),
                'jenis_obat' => $this->request->getVar('jenis_obat'),
                'nama_obat' => $this->request->getVar('nama_obat')
            ]);

            return redirect()->to('/diagnosa')->with('success', 'Data berhasil ditambahkan');
        }
}